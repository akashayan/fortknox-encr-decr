import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DecryptResult;
import com.amazonaws.services.kms.model.EncryptRequest;
import com.amazonaws.util.Base64;
import com.amazonaws.util.IOUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.SQLException;

public class AccessKeyEncr {

    private static final String awsKmsKey = "AKIATYWQQD2TJXZ4F4EV";
    private static final String awsKmsSecret = "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";
    private static final String   SECRET_KEY    = "alias/fortknox-secret";

    private static final String secretKey = "86e845b5-d7a8-423e-a69a-32ae148ce1cc";

    AWSCredentials awsCredentials = new BasicAWSCredentials(awsKmsKey, awsKmsSecret);
    AWSKMSClient kmsClient = new AWSKMSClient(awsCredentials).withRegion(Regions.AP_SOUTH_1);

    public void addAccessInfo(String accessKey, String secret, String merchantId) throws Exception {
        try {
            byte[] cipher = encrypt(secretKey, SECRET_KEY);
        } catch (Exception e) {
            throw new Exception("Some error while adding access info");
        }
    }

    public static void main(String[] args) throws SQLException, IOException {
        AccessKeyEncr obj = new AccessKeyEncr();
        byte[] cipher = IOUtils.toByteArray(AccessKeyEncr.class.getClassLoader().getResourceAsStream("dev.passwords"));
       //obj.decrypt(cipher);
       String plainText = "database.password=4BEh$=#7DL_J+=V5\n" +
               "safe.database.password=TL74h#T%t*D-*U$5\n" +
               "REGION=ap-south-1\n" +
               "juspay.freecharge.key=01E6AC8BB6694BB5A6FF4188579F6952\n" +
               "juspay.snapdeal.key=FEEEF965E32C47D29338B857E432DEC5\n" +
               "juspay.onecheck.key=E9C56C6F45844160B8061BCCE412B3AC\n" +
               "fingerprint.key=1d989e22-71b0-44ef-83ee-ac0ba95fa6b8";

        byte[] encryptedResult = obj.encrypt(plainText,SECRET_KEY);
        byte[] encoded = java.util.Base64.getEncoder().encode(encryptedResult);
        System.out.println(new String(encoded));
    }

    public byte[] encrypt(String plainTextString, String key) {
        ByteBuffer plainText = ByteBuffer.wrap(plainTextString.getBytes());
        EncryptRequest req = new EncryptRequest().withKeyId(key).withPlaintext(plainText);
        ByteBuffer encryptedText = kmsClient.encrypt(req).getCiphertextBlob();
        return encryptedText.array();
    }

    public void decrypt(byte[] cipherText) {
        cipherText = Base64.decode(cipherText);
        ByteBuffer cipherTextBlob = ByteBuffer.wrap(cipherText);
        DecryptRequest decryptReq = new DecryptRequest().withCiphertextBlob(cipherTextBlob);
        // NOTE: alias/fortknox-secret is the key used to encrypt.
        DecryptResult decryptResult = kmsClient.decrypt(decryptReq);
        System.out.println(new String(decryptResult.getPlaintext().array()));

    }


}
